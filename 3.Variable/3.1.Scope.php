<?php
/**
 * Created by PhpStorm.
 * User: Raihan
 * Date: 2016-05-15
 * Time: 12:30
 */

$a = 1;

function test(){
    $a = 5;
    echo "$a";
    echo "<br>";

    global $a;
    $b = $a;
    echo "$b";
    echo "<br>";
}
test();
echo "Global value is $a";
?>