A resource is a special variable, holding a reference to an external resource.
Resources are created and used by special functions.
See the appendix for a listing of all these functions and the corresponding resource types.
<?php
/**
 * Created by PhpStorm.
 * User: Raihan
 * Date: 2016-05-14
 * Time: 18:08
 */

// prints: mysql link
$c = mysql_connect();
echo get_resource_type($c) . "\n";

// prints: stream
$fp = fopen("foo", "w");
echo get_resource_type($fp) . "\n";

// prints: domxml document
$doc = new_xmldoc("1.0");
echo get_resource_type($doc->doc) . "\n";
?>
