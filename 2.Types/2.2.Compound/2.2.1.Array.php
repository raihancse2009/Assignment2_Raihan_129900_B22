<?php
/**
 * Created by PhpStorm.
 * User: Raihan
 * Date: 2016-05-14
 * Time: 17:28
 */

$array = array(
    "foot"=>"Shoe",
    "hand"=>"Glaps",
    "Hamlet",
    "name"=>"Raihan",
    "Love"
);
echo "<pre>";
print_r($array);
echo "</pre>";

echo $array['foot'];
echo "<pre>";
?>

Example-1 A simple array

<?php
$array1 = array(
    "foo" => "bar",
    "bar" => "foo",
);

// as of PHP 5.4
$array2 = [
    "foo" => "bar",
    "bar" => "foo",
];

echo "<pre>";
print_r($array1);
echo "</pre>";

echo "<pre>";
print_r($array2);
echo "</pre>";

?>

Example #2 Type Casting and Overwriting example

<?php
$array = array(
    1    => "a",
    "1"  => "b",
    1.5  => "c",
    true => "d",
);
var_dump($array);
?>
