Type Juggling
<br>

<p>PHP does not support explicit type definition in variable declaration;
a variable's type is determined by the context in which the variable is used.</p>

<?php
/**
 * Created by PhpStorm.
 * User: Raihan
 * Date: 2016-05-15
 * Time: 10:50
 */

$foo = "0";  // $foo is string (ASCII 48)
$foo += 2;   // $foo is now an integer (2)
$foo = $foo + 1.3;  // $foo is now a float (3.3)
$foo = 5 + "10 Little Piggies"; // $foo is integer (15)
echo $foo;
$foo = 5 + "10 Small Pigs";     // $foo is integer (15)
?>