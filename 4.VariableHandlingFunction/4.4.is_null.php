<?php
/**
 * Created by PhpStorm.
 * User: Raihan
 * Date: 2016-05-15
 * Time: 12:54
 */
$var = NULL;

// Evaluates to true because $var is empty
if (empty($var)) {
    echo '$var is either 0, empty, or not set at all'."<br>";
}

// Evaluates as true because $var is set
//if (isset($var)) {
  //  echo '$var is set even though it is empty';
//}

if(is_null($var)){
    echo '$var is not set even though it is NULL';
}
?>