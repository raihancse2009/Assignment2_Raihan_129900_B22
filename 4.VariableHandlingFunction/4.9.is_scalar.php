<?php
/**
 * Created by PhpStorm.
 * User: Raihan
 * Date: 2016-05-15
 * Time: 13:58
 */

function show_var($var)
{
    if (is_scalar($var)) {
        echo $var;
        echo "<br>";
    } else {
        echo "<pre>";
        var_dump($var);
        echo "</pre>";
    }
}
$pi = 3.1416;
$proteins = array("hemoglobin", "cytochrome c oxidase", "ferredoxin");

show_var($pi);
show_var($proteins)

?>