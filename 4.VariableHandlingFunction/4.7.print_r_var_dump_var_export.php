<?php
/**
 * Created by PhpStorm.
 * User: Raihan
 * Date: 2016-05-15
 * Time: 13:34
 */
?>
<pre>
<?php
$a = array ('a' => 'apple', 'b' => 'banana', 'c' => array ('x', 'y', 'z'));
print_r ($a);
?>
</pre>

<pre>
<?php
$a = array ('a' => 'apple', 'b' => 'banana', 'c' => array ('x', 'y', 'z'));
var_dump($a);
?>
</pre>

<pre>
<?php
$a = array ('a' => 'apple', 'b' => 'banana', 'c' => array ('x', 'y', 'z'));
var_export($a);
?>
</pre>